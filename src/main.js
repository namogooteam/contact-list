import Vue from 'vue'
import Vuex from 'vuex'
import App from './App.vue'
import VueConfirmDialog from 'vue-confirm-dialog'


Vue.config.productionTip = false
Vue.use(Vuex)
Vue.use(VueConfirmDialog)
Vue.component('vue-confirm-dialog', VueConfirmDialog.default)

const store = new Vuex.Store({
  state: {
    count: 0,
    vuex_contacts: [],
    vuex_uniqueCountries: [],
    vuex_countryCounter: 0,
    vuex_isAddDisabled: false,
    vuex_sortbycode: 0 // (0 == A-Z, 1== Z-A, 2== days until birthday)
  },
  mutations: {
    increment (state, payload) {
      state.count = payload.key1+payload.key2
    },
    updateContacts(state, contacts){
      state.vuex_contacts = contacts
    },
    updateCountries(state, countries){
      state.vuex_uniqueCountries = countries
      state.vuex_countryCounter = countries.length

    },
    updateIsAddDisabled(state, isDisabled){
      state.vuex_isAddDisabled = isDisabled
    },
    updateSortCode(state, sortCode){
      state.vuex_sortbycode = sortCode
    }


  }
})

new Vue({
  render: h => h(App),
  store: store
}).$mount('#app')